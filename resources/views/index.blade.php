<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('css/app.css')}}">
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
	<title>Enel</title>
</head>
<body>

 <div class="container">
	<div class="row justify-content-center">
		<div class="col-md-6 text-center">
			<img src="{{asset('images/Enel_Logo.png')}}" class="img-fluid text-center">
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-md-8 ">
			
			{!! Form::open(['route' => ['pesquisa.create']]) !!}				
				<div class="form-group">
					<h4>Como você classifica o atendimento das lojas físicas da Enel CE?</h4>
					{!! Form::select('name', ['Selecionar', 'Pessimo', 'Ruim', 'Regular', 'Bom', 'Ótimo'], ['class' => 'form-control']) !!}
				</div>
				<div class="form-group">
					<h4>Você já teve oportunidade de utilizar os nosso canais virtuais?</h4>
					{!! Form::label('description', 'Sim', array('class' => 'control-label')) !!}
					{!! Form::checkbox('status', null, false) !!}								
				</div>
				<div class="form-group">
					{!! Form::label('open', 'Não', array('class' => 'control-label')) !!}
					{!! Form::checkbox('status', null, false) !!}
				</div>
				<div class="form-group">
					<h4>Você está satisfeito com o serviço oferecido?</h4>
					{!! Form::label('description', 'Sim', array('class' => 'control-label')) !!}
					{!! Form::checkbox('status', null, false) !!}								
				</div>
				<div class="form-group">
					{!! Form::label('open', 'Não', array('class' => 'control-label')) !!}
					{!! Form::checkbox('status', null, false) !!}
				</div>
				<div class="form-group">
					<h4>Já houve alguma queda de Energia em sua residência?</h4>
					{!! Form::label('description', 'Sim', array('class' => 'control-label')) !!}
					{!! Form::checkbox('status', null, false) !!}								
				</div>
				<div class="form-group">
					{!! Form::label('open', 'Não', array('class' => 'control-label')) !!}
					{!! Form::checkbox('status', null, false) !!}
				</div>
				<div class="form-group">
					<h4>Se sim, o atendimento da equipe em campo foi imediato?</h4>
					{!! Form::label('description', 'Sim', array('class' => 'control-label')) !!}
					{!! Form::checkbox('status', null, false) !!}								
				</div>
				<div class="form-group">
					{!! Form::label('open', 'Não', array('class' => 'control-label')) !!}
					{!! Form::checkbox('status', null, false) !!}
				</div>
				<div class="form-group">
					<h4>Deixe aqui seu comentário.</h4>
					{!! Form::textarea('credence', null, ['class' => 'form-control']) !!}
				</div>
				{!! Form::submit('Enviar', ['class' => 'btn btn-primary']); !!}
			{!! Form::close() !!}
		</div>
	</div>
 </div>
<br><br>
</body>
</html>
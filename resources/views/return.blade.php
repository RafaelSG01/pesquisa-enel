<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('css/app.css')}}">
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
	<title>ENEL</title>
</head>
<body>
	<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-6 text-center">
			<img src="{{asset('images/Enel_Logo.png')}}" class="img-fluid text-center">
		</div>
	</div>
		<div class="row justify-content-center">
			<div class="col-md-6 text-center">
				<h2>Obrigado por participar da nossa pesquisa de satisfação.</h2> 
				<h4>Em forma de agradecimento, queremos retribuir com um mimo feito especialmente para você.</h4>
			</div>
		</div>
	</div>
</body>
</html>